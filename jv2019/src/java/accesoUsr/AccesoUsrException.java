/** 
Proyecto: Juego de la vida.
 * Clase de excepción para la gestión de los errores 
 * en las clases del interfaz de usuario.
 * @since: prototipo 0.1.2
 * @source: AccesoUsrException.java 
 * @version: 0.1.2 - 2020/02/25
 * @author: ajp
 */

package accesoUsr;

public class AccesoUsrException extends RuntimeException {

	public AccesoUsrException(String mensaje) {
		super(mensaje);
	}
	
	public AccesoUsrException() {
		super();
	}
}
