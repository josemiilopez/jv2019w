/** 
Proyecto: Juego de la vida.
 * Organiza y gestiona la configuración de la aplicación.
 * Utiliza Properties para organizar y almacenar la configuración de manera persistente en un fichero.
 * Aplica una variante del patrón Singleton.
 * @since: prototipo 0.2.0
 * @source: Configuracion.java 
 * @version: 0.2.0 - 2020/03/30
 * @author: ajp
 * @author: JGR, JJMZ, TIP, FGCB
 */

package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Configuracion {

	// Singleton.
	private static Properties configuracion;			// Hashtable <String, String> con persistencia en fichero.
	private File fConfig;						// Fichero de datos.

	/**
	 * Método estático de acceso a la instancia única.
	 * Si no existe la crea invocando al contructor privado.
	 * Utiliza incialización diferida.
	 * Sólo se crea una vez; instancia única -patrón singleton-
	 * @retun instancia
	 */
	public static Properties get() {
		if (configuracion == null) {
			new Configuracion();
		}
		return configuracion;
	}
	
	/**
	 * Constructor por defecto de uso interno.
	 */
	private Configuracion() {
		configuracion = new Properties();
		fConfig = obtenerRutaFichero();
		try {
			InputStream is = new FileInputStream(fConfig);
			if (fConfig.exists()) {
				configuracion.load(is);			// Carga configuración desde el fichero.
			}
			else {
				cargarDatosPredeterminados();	// La primera ejecución genera en fichero la config predeterminada.
				guardar();
			}
			is.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Guarda configuración al fichero.
	 */
	public void guardar() {
		try {
			OutputStream os = new FileOutputStream(fConfig);
			configuracion.store(os, "Configuracion actualizada...");
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	private void cargarDatosPredeterminados() {
		configuracion.put("aplicacion.titulo", "JV2019");
		
		configuracion.put("mundo.nombrePredeterminado", "Demo0");
		configuracion.put("mundo.sizePredeterminado", "1");
		configuracion.put("mundo.tipoPredeterminado", "PLANO");
		configuracion.put("simulacion.ciclosPredeterminados", "35");
		configuracion.put("fecha.predeterminadaFija", "2002.02.02");
		
		configuracion.put("usuario.admin", "Admin");
		configuracion.put("usuario.nifAdmin", "00000000T");
		configuracion.put("usuario.nombrePredeterminado", "Invitado");
		configuracion.put("usuario.fechaNacimientoPredeterminada", "2001.01.01");
		configuracion.put("usuario.edadMinima", "16");

		configuracion.put("nif.predeterminado", "00000001R");
		configuracion.put("correo.dominioPredeterminado", "@gmail.com");
		configuracion.put("direccion.predeterminada", "Iglesia,0,30012,Murcia");
		configuracion.put("claveAcceso.predeterminada", "Miau#0");
		
		configuracion.put("sesion.intentosPermitidos", "3");
		
		configuracion.put("dados.nombreDirectorio", "datos");
		
		configuracion.put("usuarios.nombreFichero", "usuarios.dat");
		configuracion.put("equivalenciasId.nombreFichero", "equivalId.dat");
		configuracion.put("sesiones.nombreFichero", "sesiones.dat");
		configuracion.put("mundos.nombreFichero", "mundos.dat");
		configuracion.put("patrones.nombreFichero", "patrones.dat");
		configuracion.put("simulaciones.nombreFichero", "simulaciones.dat");
		
		configuracion.put("db4o.nombreFicheroDB", "JVdatos.db4o");
		configuracion.put("mySql.url1", "jdbc:mysql://172.20.254.134/jvdatos");
		
	}

	private File obtenerRutaFichero() {
		new File("datos").mkdirs();				// nombre de directorio.
		return new File("." + File.pathSeparator + "datos" + File.separator + "jv2019.cfg");
	}
}