/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de SesionUsuario según el modelo 1.2
 *  @since: prototipo 0.1.0
 *  @source: Sesion.java 
 *  @version: 0.1.2 - 2020/02/17 
 */

package modelo;

import util.Fecha;

public class Sesion implements Identificable {

	public enum EstadoSesion {
		ACTIVA,
		CERRADA
	}

	private Usuario usr;
	private Fecha fecha;
	private EstadoSesion estado;

	/**
	 * Constructor convencional. Utiliza métodos set...()
	 * @param usr
	 * @param fecha
	 */
	public Sesion(Usuario usr, Fecha fecha) {
		setUsr(usr);
		setFecha(fecha);
		estado = EstadoSesion.ACTIVA;
	}

	/**
	 * Constructor por defecto. Utiliza constructor convencional.
	 * @throws ModeloException 
	 */
	public Sesion() throws ModeloException {
		this(new Usuario(), new Fecha());
	}

	/**
	 * Constructor copia.
	 * @param sesion
	 * @throws ModeloException 
	 */
	public Sesion(Sesion sesion) throws ModeloException {
		this.usr = new Usuario(sesion.usr);
		this.fecha = sesion.fecha.clone();
		// El estado definido con el constructor convencional hay que actualizarlo.
		this.estado = sesion.estado;
	}

	public Sesion(Usuario usr) {
		this(usr, new Fecha());
	}

	/**
	 * Sintetiza un identificador único concatenando el id de usuario y una marca de tiempo de 14 dígitos.
	 * @return el id de sesion generado.
	 */
	@Override
	public String getId() {
		return usr.getId() + ":" + fecha.toStringMarcaTiempo();
	}
	
	public Usuario getUsr() {
		return usr;
	}

	public void setUsr(Usuario usr) {
		if (usr != null) {
			this.usr = usr;
			return;
		}
		throw new ModeloException("Sesion.usr: null...");	
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		if (fecha != null) {
			this.fecha = fecha;
			return;
		}
		throw new ModeloException("Sesion.fecha: null...");
	}
	
	public EstadoSesion getEstado() {
		return estado;
	}
	
	/**
	 * Redefine el método heredado de la clase Object.
	 * @return el texto formateado del estado (valores de atributos) 
	 * del objeto de la clase SesionUsuario  
	 */
	@Override
	public String toString() {
		return String.format("%-16s %s\n "
						+ "%-16s %s\n "
						+ "%-16s %s\n", 
						"usuario:", usr.getId(),
						"fecha:", fecha, 
						"estado:", estado);	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((usr == null) ? 0 : usr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sesion other = (Sesion) obj;
		if (estado != other.estado)
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (usr == null) {
			if (other.usr != null)
				return false;
		} else if (!usr.equals(other.usr))
			return false;
		return true;
	}

	@Override
	public Sesion clone() {
		try {
			return new Sesion(this);
		} catch (ModeloException e) {
			e.printStackTrace();
		}
		return null;
	}
	
} 
